use clap::Parser;
use gpio::GpioOut;
use std::thread::sleep;
use std::time::Duration;

/// Start or Close fan for rpi
#[derive(Debug, Parser)]
#[clap(author, about)]
struct Arg {
    /// When temp is more than it, open fan
    #[clap(short, long, default_value_t = 48)]
    high_temp: u8,

    /// When temp is less than it, close fan
    #[clap(short, long, default_value_t = 45)]
    low_temp: u8,

    /// GPIO Number
    #[clap(short, long, default_value_t = 18)]
    channel: u16,

    /// interval for geeting temp
    #[clap(short, long, default_value_t = 5)]
    interval: u64,

    /// Temp file
    #[clap(short, long, default_value_t = String::from("/sys/class/thermal/thermal_zone0/temp"))]
    file: String,
}

fn main() {
    let arg = Arg::parse();
    let mut gpio = gpio::sysfs::SysFsGpioOutput::open(arg.channel).unwrap();
    // close fan
    gpio.set_low().unwrap();

    let mut is_closed = true;
    loop {
        let temp = get_temp(arg.file.as_str());
        if is_closed {
            if temp > arg.high_temp {
                println!("Open Fan. temp == {}", temp);
                gpio.set_high().unwrap();
                is_closed = !is_closed;
            }
        } else {
            if temp < arg.low_temp {
                println!("Close Fan. temp == {}", temp);
                gpio.set_low().unwrap();
                is_closed = !is_closed;
            }
        }
        // sleep
        sleep(Duration::from_secs(arg.interval));
    }
}

fn get_temp(temp_file: &str) -> u8 {
    let s = std::fs::read_to_string(temp_file).unwrap();
    let temp: usize = s.trim().parse().unwrap();
    (temp / 1000) as u8
}
